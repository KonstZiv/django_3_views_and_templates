from time import strftime
from django.http import HttpResponse, HttpRequest
from django.template import Context, Template
from django.shortcuts import render
from datetime import date

def citizen_kane(request: HttpRequest) -> HttpResponse:
    content = """{{movie}} was realized in {{year}}"""
    template = Template(content)
    context = Context({'movie': 'Citizen Kane', 'year': 1941})

    result = template.render(context)
    return HttpResponse(result)

def citizen_kane_f(request: HttpRequest) -> HttpResponse:
    return render(request, 'simple.txt', {'movie': 'Casablanca', 'year': 1942})

def maltese_falcon(request: HttpRequest) -> HttpResponse:
    return render(
        request,
        'falcon.html',
        {'movie': 'Maltese Falcon', 'year': 1941},
    )

def psycho(request: HttpRequest) -> HttpResponse:
    data = {
        'movie': 'Psycho',
        'year': 1960,
        'is_scary': True,
        'color': False,
        'tomato_meter': 95,
        'tomato_audience': 95,
    }
    return render(request, 'psycho.html', data)



# Create your views here.
